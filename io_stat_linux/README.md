# Description
## A ***Non-invasive*** library to collect process io stat to a `linux process` and its sub processes.
# Usage
```
env LD_PRELOAD=$(realpath libhook.so) [env hookOutputPath=$(realpath ../)] ${YOUR-BINARY-PATh} ${PARAM-to-YOUR-BINARY}
```
## build libhook.so
```shell
g++ hook.cpp -lstdc++ -Wl,--no-as-needed -ldl -shared -fPIC -o libhook.so
```
## Run example
### build example
```
g++ example.cpp -o example
```
### run
```
env LD_PRELOAD=$(realpath libhook.so) env hookOutputPath=$(realpath ../) ls -a
env LD_PRELOAD=$(realpath libhook.so) ls -a
env LD_PRELOAD=~/libhook.so ls -l
env LD_PRELOAD=$(realpath libhook.so) ./example
```