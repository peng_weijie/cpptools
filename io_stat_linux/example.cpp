
/*****************************************************************************************************************************
 *
 *             the io result will be output to a file named res.txt in current dir.
 *
 *                  [processName]     [process run time]      [read count]   [write count]   [close count]    [lseek count]
 * finial output:   [test]          total run time(ms): 3       read: 2        write: 2        close: 3        lseek: 2 
 * 
 * Auther: weijiepeng<pmode2018@163.com>
 * **************************************************************************************************************************/

#include <fcntl.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <cstring>
#include <iostream>

char* getPath() {
  auto currentDir = getcwd(NULL, 0);
  auto path = new char[strlen(currentDir) + 2 + strlen("output.txt")];
  sprintf(path, "%s/output.txt", currentDir);
  return path;
}

void testWrite() {
  auto path = getPath();
  auto fd = open(path, O_CREAT | O_RDWR, 0777);
  delete[] path;
  const char* buff = "this is a test output";
  write(fd, buff, strlen(buff));
  close(fd);
}

void testRead() {
  auto path = getPath();
  auto fd = open(path, O_CREAT | O_RDWR, 0777);
  delete[] path;
  char* buff = new char[10000];
  read(fd, buff, 10000);
  std::cout << buff << std::endl;
  delete[] buff;
  close(fd);
}

void testSeekAndReadWrite() {
  auto path = getPath();
  auto fd = open(path, O_CREAT | O_RDWR, 0777);
  delete[] path;
  lseek(fd, 5, SEEK_SET);
  char* buff = new char[10000];
  read(fd, buff, 10000);
  std::cout << buff << std::endl;
  delete[] buff;
  lseek(fd, 8, SEEK_SET);
  write(fd, "override", strlen("override"));
  close(fd);
}

int main() {
  testWrite();
  testRead();
  testSeekAndReadWrite();
  return 0;
}