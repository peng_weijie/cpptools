/******************************************************************************
 *  description:
 *    if `env.hookOutputPath` not set, result and log will be output to current
 * work dir.
 *    if `env.hookOutputPath` set, result and log will be output to the
 * folder input. But you'd better ensure the folder exists, or no result will be
 * output.
 *
 *   usage:
 *     env LD_PRELOAD=./libhook.so env hookOutputPath=$(realpath ../) ls
 *     env LD_PRELOAD=./libhook.so env hookOutputPath=$(realpath ../) ls -a
 *
 *  Auther: weijiepeng<pmode2018@163.com>
 * ***************************************************************************/
#include <dlfcn.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include <atomic>
#include <chrono>
#include <cstring>
#include <fstream>
#include <iostream>
#include <memory>

#include "stdarg.h"

extern char *program_invocation_short_name;
/**
 * @brief
 * sample interval(s), if the process costs more than sample_interval seconds,
 * we will create a new log file to it. filename: `processName_pid.txt`, and the
 * process will add a new record every sample_interval seconds.
 */
const int sample_interval = 2;

/* could not hook `open` so far. but you can get the approximate count from
 * `close`.
 */
// typedef int (*open_pfn_t)(const char* path, int oflag, ...);
// static open_pfn_t g_sys_open_func = (open_pfn_t)dlsym(RTLD_NEXT, "open");
// static std::atomic_int64_t open_counter(0);
// __inline __attribute__((__always_inline__)) int open(const char* path,
//                                                      int oflag, ...) {
//   open_counter.fetch_add(1);
//   return g_sys_open_func(path, oflag, __builtin_va_arg_pack());
// }

typedef ssize_t (*read_pfn_t)(int fildes, void *buf, size_t nbyte);
static read_pfn_t g_sys_read_func = (read_pfn_t)dlsym(RTLD_NEXT, "read");
static std::atomic_int64_t read_counter(0);
ssize_t read(int fildes, void *buf, size_t nbyte) {
  read_counter.fetch_add(1);
  return g_sys_read_func(fildes, buf, nbyte);
}

typedef ssize_t (*write_pfn_t)(int fd, const void *buf, size_t nbytes);
static write_pfn_t g_sys_write_func = (write_pfn_t)dlsym(RTLD_NEXT, "write");
static std::atomic_int64_t write_counter(0);
ssize_t write(int fildes, const void *buf, size_t nbyte) {
  write_counter.fetch_add(1);
  return g_sys_write_func(fildes, buf, nbyte);
}

typedef int (*close_pfn_t)(int fd);
static close_pfn_t g_sys_close_func = (close_pfn_t)dlsym(RTLD_NEXT, "close");
static std::atomic_int64_t close_counter(0);
int close(int fildes) {
  close_counter.fetch_add(1);
  return g_sys_close_func(fildes);
}

typedef off_t (*lseek_pfn_t)(int fildes, off_t offset, int whence);
static lseek_pfn_t g_sys_lseek_func = (lseek_pfn_t)dlsym(RTLD_NEXT, "lseek");
static std::atomic_int64_t lseek_counter(0);
off_t lseek(int fildes, off_t offset, int whence) {
  lseek_counter.fetch_add(1);
  return g_sys_lseek_func(fildes, offset, whence);
}

static std::time_t timestamp_start;
static std::time_t timestamp_end;

static std::time_t getTimestamp() {
  std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>
      tp = std::chrono::time_point_cast<std::chrono::milliseconds>(
          std::chrono::system_clock::now());
  return tp.time_since_epoch().count();
}

static char *getOutputDir(const char *const suffix) {
  char *path = getenv("hookOutputPath");
  char *outputPath = 0;
  if (path && strlen(path) > 1) {
    outputPath = path;
  } else {
    outputPath = getcwd(NULL, 0);
  }
  char *res = new char[strlen(outputPath) + strlen(suffix) + 1];
  sprintf(res, "%s/%s", outputPath, suffix);
  return res;
}

/**
 * DON't call `cout` in this library. Because if the target process call an
 * other process and parse to stdout.
 * */
static void log(const char *const msg) {
  static std::shared_ptr<char> path(getOutputDir("log.txt"),
                                    [](char *x) { delete[] x; });
  static std::shared_ptr<std::ofstream> log(
      new std::ofstream(path.get(), std::ios::app), [](std::ofstream *x) {
        x->flush();
        x->close();
        delete x;
      });
  *log << "[" << program_invocation_short_name << "](" << getpid() << ") "
       << msg << std::endl;
}

static void showLogInterval() {
  auto pid = getpid();
  char *suffix = new char[strlen(program_invocation_short_name) + strlen("_") +
                          sizeof(int) + strlen(".txt") + 1];
  sprintf(suffix, "%s_%d.txt", program_invocation_short_name, pid);
  static std::shared_ptr<char> path(getOutputDir(suffix),
                                    [](char *x) { delete[] x; });
  delete[] suffix;
  static std::shared_ptr<std::ofstream> out(
      new std::ofstream(path.get(), std::ios::app), [](std::ofstream *x) {
        x->flush();
        x->close();
        delete x;
      });
  log(path.get());
#define concat(func) #func << ": " << func##_counter.load() << " "
  *out
      // << concat(open)
      << concat(read) << concat(write) << concat(close) << concat(lseek)
      << std::endl;
}

static void handle_sigalrm(int sig) {
  showLogInterval();
  alarm(sample_interval);
  signal(SIGALRM, handle_sigalrm);
}

__attribute__((constructor)) static void lib_load() {
  alarm(sample_interval);
  signal(SIGALRM, handle_sigalrm);
  timestamp_start = getTimestamp();
}

__attribute__((destructor)) static void lib_unload() {
  timestamp_end = getTimestamp();
  auto path = getOutputDir("res.txt");
  std::ofstream out(path, std::ios::app);
  delete[] path;
#define concat(func) #func << ": " << func##_counter.load() << " "
  out << "[" << program_invocation_short_name << "] "
      << "total run time(ms): " << timestamp_end - timestamp_start
      << " "
      // << concat(open)
      << concat(read) << concat(write) << concat(close) << concat(lseek)
      << std::endl;
  out.close();
}
