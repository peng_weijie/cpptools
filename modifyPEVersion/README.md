# CPP_Tools

#### 介绍
C ++ 工具——————修改PE文件版本信息

#### 软件架构
工具分为三部分，core、gui、console；
core：      修改版本号的核心代码逻辑，为静态库；
gui：       GUI程序，使用Qt的GUI创建界面，调用core的方法进行修改版本号
console：   控制台程序，通过命令行的方式来修改版本号，不依赖于QT的环境


#### 安装教程

1.  cd cmake
2.  call generate_Sln_${platform}.bat
3.  cd ../../build/Sln/${platform}/RelWithDebInfo
4.  open modifyPE.sln and build

#### 使用说明

1.  console 
        参数：除了filePath外， 其他均为可选参数
            --filePath="C:\Program Files\xxxx.exe" --productName=我的软件 --productVersion=我的版本 --company=我的公司 --inner=inner.exe --origin=origin.exe --trademark=我的商标 --comment=备注 --description=描述 --version=1.6666.2333.5555 --copyright=我的版权

2.  gui 双击运行

