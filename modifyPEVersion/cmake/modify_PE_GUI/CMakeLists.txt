##设置库名称
set(LIBRARY_TARGET_NAME modify_PE_GUI)
##查找所有头文件
file(GLOB_RECURSE  ${LIBRARY_TARGET_NAME}_HEADER_FILES
    LIST_DIRECTORIES False 
    "${PROJECT_SOURCE_DIR}/${LIBRARY_TARGET_NAME}/*.h"
)
##设置VS筛选器，头文件分文件夹
source_group(
    TREE "${PROJECT_SOURCE_DIR}/${LIBRARY_TARGET_NAME}"
    ##PREFIX "Header Files"
    FILES ${${LIBRARY_TARGET_NAME}_HEADER_FILES}
)

##查找所有源文件
file(GLOB_RECURSE  ${LIBRARY_TARGET_NAME}_SRC_FILES
    LIST_DIRECTORIES False 
    "${PROJECT_SOURCE_DIR}/${LIBRARY_TARGET_NAME}/*.cpp"
)
##设置VS筛选器，源码分文件夹
source_group(
    TREE "${PROJECT_SOURCE_DIR}/${LIBRARY_TARGET_NAME}"
    ##PREFIX "Source Files"
    FILES ${${LIBRARY_TARGET_NAME}_SRC_FILES}
)

##设置ts文件路径
SET(${LIBRARY_TARGET_NAME}_TS_FILE ${PROJECT_SOURCE_DIR}/${LIBRARY_TARGET_NAME}_zh_CN.ts)
message(${${LIBRARY_TARGET_NAME}_TS_FILE})
##设置ts文件输出目录,用来控制qm文件输出目录
set_source_files_properties(${${LIBRARY_TARGET_NAME}_TS_FILE} PROPERTIES OUTPUT_LOCATION ${PROJECT_SOURCE_DIR}/../Build/bin/${PROJECT_PLATFORM}/$(Configuration))

##查找Qt翻译家
find_package( Qt5LinguistTools )
##创建/更新ts文件并提供qm生成动作
QT5_CREATE_TRANSLATION(${LIBRARY_TARGET_NAME}_QM_FILE
    ${${LIBRARY_TARGET_NAME}_HEADER_FILES}
    ${${LIBRARY_TARGET_NAME}_SRC_FILES}
    ${${LIBRARY_TARGET_NAME}_TS_FILE}
)

##设置生成目标
add_executable(${LIBRARY_TARGET_NAME}
    ${${LIBRARY_TARGET_NAME}_HEADER_FILES}
    ${${LIBRARY_TARGET_NAME}_SRC_FILES}
    ${${LIBRARY_TARGET_NAME}_QM_FILE}
)

set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /SUBSYSTEM:WINDOWS")

set_target_properties(${LIBRARY_TARGET_NAME} PROPERTIES WIN32_EXECUTABLE ON)

set_target_properties(${LIBRARY_TARGET_NAME} PROPERTIES LINK_FLAGS "/SAFESEH:NO /LARGEADDRESSAWARE")


##指定工程生成文件组织结构
#set_target_properties(${LIBRARY_TARGET_NAME} PROPERTIES FOLDER ${FOLDER_NAME})

##打开qt特性配置
set_target_properties(${LIBRARY_TARGET_NAME} PROPERTIES AUTOMOC ON)
set_target_properties(${LIBRARY_TARGET_NAME} PROPERTIES AUTOUIC ON)
set_target_properties(${LIBRARY_TARGET_NAME} PROPERTIES AUTORCC ON)

##设置预处理器定义
if(${PROJECT_PLATFORM} STREQUAL "Win32")
	target_compile_definitions(${LIBRARY_TARGET_NAME} PRIVATE UNICODE WIN32 QT_DLL QT_NO_DEBUG NDEBUG QT_CORE_LIB)
elseif(${PROJECT_PLATFORM} STREQUAL "x64")
	target_compile_definitions(${LIBRARY_TARGET_NAME} PRIVATE UNICODE WIN64 QT_DLL QT_NO_DEBUG NDEBUG QT_CORE_LIB)
endif()
##配置构建/使用时的头文件路径
target_include_directories(
    ${LIBRARY_TARGET_NAME}
    PUBLIC   
    "$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/modify_PE_core/include>"
)

##配置库依赖
find_package(Qt5 COMPONENTS Core Gui Widgets REQUIRED)
target_link_libraries(${LIBRARY_TARGET_NAME}
    PRIVATE Qt5::Core Qt5::Gui Qt5::Widgets 
    PRIVATE ${PROJECT_NAME}::modify_PE_core
)

#COMMAND cmd /c ${PROJECT_SOURCE_DIR}/cmake/cmd/CopyAllQt.cmd "${QT_PREFIX_PATH}/bin" ${PROJECT_SOURCE_DIR}/../build/bin/${PROJECT_PLATFORM}/$<CONFIG>
add_custom_command(TARGET ${LIBRARY_TARGET_NAME} POST_BUILD 
    #COMMAND cmd /c if not exist ${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>/platforms md ${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>/platforms
    COMMAND if not exist \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>/platforms\" echo d | XCOPY  /e /y /i /f  \"${QT_PREFIX_PATH}/plugins/platforms\" \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>/platforms/\" /s 
    COMMAND if not exist \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>/Qt5Core.dll\"  XCOPY  /e /y /i /f \"${QT_PREFIX_PATH}/bin/Qt5Core.dll\" \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>\"
    COMMAND if not exist \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>/Qt5Gui.dll\"  XCOPY  /e /y /i /f \"${QT_PREFIX_PATH}/bin/Qt5Gui.dll\" \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>\"
    COMMAND if not exist \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>/Qt5Widgets.dll\" XCOPY  /e /y /i /f \"${QT_PREFIX_PATH}/bin/Qt5Widgets.dll\" \"${PROJECT_SOURCE_DIR}../build/bin/${PROJECT_PLATFORM}/$<CONFIG>\"
)
