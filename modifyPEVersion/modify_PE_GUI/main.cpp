#include "modifyPEVersion.h"
#include <QtWidgets/QApplication>
#include "iostream"
#include "QtWidgets/QMessageBox"
#include "QFile"
#include "CVersionUpdater.h"
#include <atlstr.h>
#include "CStringFileInfo.h"
#include "CStringTable.h"
#include "stdafx.h"
#include <minwindef.h>
int main(int argc, char *argv[])
{
	if (argc == 3)
	{
		CVersionUpdater updater;
		if (updater.Open(ATL::CString(argv[1])))
		{
			if (updater.VersionInfo == nullptr)
			{
				updater.VersionInfo = new CVersionInfo;
			}
			if (updater.VersionInfo->StringFileInfo == NULL)
				updater.VersionInfo->StringFileInfo = new CStringFileInfo;
			if (updater.VersionInfo->StringFileInfo->Children.size() < 0) 
				updater.VersionInfo->StringFileInfo->Children.push_back(new CStringTable);
			for (CStringTable* st : updater.VersionInfo->StringFileInfo->Children)
			{
				//st->SetComments(_T(""));
				//st->SetCompanyName(_T("Glodon limited"));
				// 			st->SetFileDescription(_T(""));
				st->SetFileVersion(QStringToTCHAR(QString::fromLocal8Bit(argv[2])));
				// 			st->SetInternalName(_T(""));
				//st->SetLegalCopyright(_T("Glodon limited"));
				// 			st->SetLegalTrademarks(_T(""));
				// 			st->SetOriginalFilename(_T(""));
				// 			st->SetPrivateBuild(_T(""));
				// 			st->SetProductName(_T(""));
				st->SetProductVersion(QStringToTCHAR(QString::fromLocal8Bit(argv[2])));
				// 			st->SetSpecialBuild(_T(""));
			}
			auto versionList = QString::fromLocal8Bit(argv[2]).split(".");

			updater.VersionInfo->FixedFileInfo.dwFileVersionMS = MAKELONG(versionList[1].toInt(), versionList[0].toInt());
			updater.VersionInfo->FixedFileInfo.dwFileVersionLS = MAKELONG(versionList[3].toInt(), versionList[2].toInt());
			updater.VersionInfo->FixedFileInfo.dwProductVersionMS = MAKELONG(versionList[1].toInt(), versionList[0].toInt());
			updater.VersionInfo->FixedFileInfo.dwProductVersionLS = MAKELONG(versionList[3].toInt(), versionList[2].toInt());
			updater.Update();
		}

		//ModifyPEOperator  *pOperator = new ModifyPEOperator(QString::fromLocal8Bit(argv[1]), QString::fromLocal8Bit(argv[2]));
		//pOperator->doModifyVersion();
	}
	else {
		QApplication a(argc, argv);
		modifyPEVersion w;
		w.show();
		return a.exec();
	}
}
