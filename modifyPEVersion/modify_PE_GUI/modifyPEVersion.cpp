#include "modifyPEVersion.h"
#include "QFileDialog"
#include "QLineEdit"
#include "QString"
#include "QPushButton"
#include "QLabel"
#include "QVBoxLayout"
#include "QHBoxLayout"
#include "QTabWidget"
#include "QDialogButtonBox"
#include "QMessageBox"

modifyPEVersion::modifyPEVersion(QWidget *parent)
    : QMainWindow(parent)
{
	m_pTabWidget = new QTabWidget();
	this->setCentralWidget(m_pTabWidget);
	initVersionMap();
	initVersionUI();
}

void modifyPEVersion::initVersionUI()
{
	QWidget *pMainWidget = new QWidget();
	m_pTabWidget->addTab(pMainWidget, QStringLiteral("版本管理"));
	QVBoxLayout *pMainLayout = new QVBoxLayout();
	pMainWidget->setLayout(pMainLayout);

	//第一行
	init1stLineUI(pMainLayout);
	QFrame *line = new QFrame();
	line->setFixedHeight(5);
	line->setFrameShape(QFrame::HLine);        // 水平分割线
	line->setFrameShadow(QFrame::Sunken);
	pMainLayout->addWidget(line);
	
	for (auto versionField : mapVersionFields.keys())
	{
		initEachLineUI(pMainLayout, versionField);
	}

	//最后一行
	initLastLineUI(pMainLayout);
}

void modifyPEVersion::initVersionMap()
{
	mapVersionFields[QStringLiteral("备注")] = new QLineEdit();
	mapVersionFields[QStringLiteral("公司名称")] = new QLineEdit();
	mapVersionFields[QStringLiteral("文件描述")] = new QLineEdit();
	mapVersionFields[QStringLiteral("文件版本")] = new QLineEdit();
	mapVersionFields[QStringLiteral("内部名称")] = new QLineEdit();
	mapVersionFields[QStringLiteral("版权")] = new QLineEdit();
	mapVersionFields[QStringLiteral("合法商标")] = new QLineEdit();
	mapVersionFields[QStringLiteral("原始文件名")] = new QLineEdit();
	mapVersionFields[QStringLiteral("产品名称")] = new QLineEdit();
	mapVersionFields[QStringLiteral("产品版本")] = new QLineEdit();
}

void modifyPEVersion::initEachLineUI(QVBoxLayout *pMainLayout, QString lableName)
{
	QHBoxLayout *pLayout = new QHBoxLayout();
	pMainLayout->addLayout(pLayout);
	QLabel *label = new QLabel(lableName + QStringLiteral("："));
	label->setFixedWidth(70);
	pLayout->addWidget(label, 0);
	pLayout->addWidget(mapVersionFields[lableName]);
}

void modifyPEVersion::init1stLineUI(QVBoxLayout *pMainLayout)
{
	//第一行
	QHBoxLayout *pFirstLineLayout = new QHBoxLayout();
	pMainLayout->addLayout(pFirstLineLayout);
	QLabel *lblPath = new QLabel(QStringLiteral("文件路径："));
	pFirstLineLayout->addWidget(lblPath);
	m_pLinePath = new QLineEdit();
	pFirstLineLayout->addWidget(m_pLinePath);
	QPushButton *pBtnOpenFile = new QPushButton(QStringLiteral("打开"));
	pFirstLineLayout->addWidget(pBtnOpenFile);
	connect(pBtnOpenFile, &QPushButton::clicked, this, &modifyPEVersion::onClickedOpenFile);
}

void modifyPEVersion::initLastLineUI(QVBoxLayout *pMainLayout)
{
	QHBoxLayout *pBtnLayout = new QHBoxLayout();
	pBtnLayout->addStretch();
	pMainLayout->addLayout(pBtnLayout);
	QPushButton *btnOk = new QPushButton(QStringLiteral("确定"));
	connect(btnOk, &QPushButton::clicked, this, &modifyPEVersion::onModifyVersionClicked);

	pBtnLayout->addWidget(btnOk);
	QPushButton *btnCancel = new QPushButton(QStringLiteral("退出"));
	pBtnLayout->addWidget(btnCancel);
}

void modifyPEVersion::onClickedOpenFile()
{
	m_strFileDir = QFileDialog::getOpenFileName(this, QStringLiteral("选择文件")
		, "C:\\Program Files\\Glodon\\GTJ2021\\bin"
		, "exe(*.exe);;dll(*.dll)");
	std::string ss = m_strFileDir.toLocal8Bit().data();
	
	if (updater.Open(ss.c_str()))
	{
		if (updater.VersionInfo == nullptr)
		{
			updater.VersionInfo = new CVersionInfo;
		}
		if (updater.VersionInfo->StringFileInfo == NULL)
			updater.VersionInfo->StringFileInfo = new CStringFileInfo;
		if (updater.VersionInfo->StringFileInfo->Children.size() < 0)
			updater.VersionInfo->StringFileInfo->Children.push_back(new CStringTable);
		if (!updater.VersionInfo->StringFileInfo->Children.empty())
		{
			auto item = updater.VersionInfo->StringFileInfo->Children[0];
			if (const ATL::CString* cs  = item->GetComments())
			{
				mapVersionFields[QStringLiteral("备注")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetCompanyName())
			{
				mapVersionFields[QStringLiteral("公司名称")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetFileDescription())
			{
				mapVersionFields[QStringLiteral("文件描述")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetFileVersion())
			{
				mapVersionFields[QStringLiteral("文件版本")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetInternalName())
			{
				mapVersionFields[QStringLiteral("内部名称")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetLegalCopyright())
			{
				mapVersionFields[QStringLiteral("版权")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetOriginalFilename())
			{
				mapVersionFields[QStringLiteral("原始文件名")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetProductName())
			{
				mapVersionFields[QStringLiteral("产品名称")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetProductVersion())
			{
				mapVersionFields[QStringLiteral("产品版本")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			if (const ATL::CString* cs = item->GetLegalTrademarks())
			{
				mapVersionFields[QStringLiteral("合法商标")]->setText(QString::fromWCharArray((LPCTSTR)(*cs), cs->GetLength()));
			}
			m_pLinePath->setReadOnly(true);
			m_pLinePath->setText(m_strFileDir);
		}
	}
}

void modifyPEVersion::onModifyVersionClicked()
{
	for (CStringTable* st : updater.VersionInfo->StringFileInfo->Children)
	{
		st->SetComments(QStringToTCHAR(mapVersionFields[QStringLiteral("备注")]->text().trimmed()));
		st->SetCompanyName(QStringToTCHAR(mapVersionFields[QStringLiteral("公司名称")]->text().trimmed()));
		st->SetFileDescription(QStringToTCHAR(mapVersionFields[QStringLiteral("文件描述")]->text().trimmed()));
		st->SetFileVersion(QStringToTCHAR(mapVersionFields[QStringLiteral("文件版本")]->text().trimmed()));
		st->SetInternalName(QStringToTCHAR(mapVersionFields[QStringLiteral("内部名称")]->text().trimmed()));
		st->SetLegalCopyright(QStringToTCHAR(mapVersionFields[QStringLiteral("版权")]->text().trimmed()));
		st->SetLegalTrademarks(QStringToTCHAR(mapVersionFields[QStringLiteral("合法商标")]->text().trimmed()));
		st->SetOriginalFilename(QStringToTCHAR(mapVersionFields[QStringLiteral("原始文件名")]->text().trimmed()));
		//st->SetPrivateBuild(QStringToTCHAR(QStringLiteral("ffffff")));
		st->SetProductName(QStringToTCHAR(mapVersionFields[QStringLiteral("产品名称")]->text().trimmed()));
		st->SetProductVersion(QStringToTCHAR(mapVersionFields[QStringLiteral("产品版本")]->text().trimmed()));
		//st->SetSpecialBuild(QStringToTCHAR(QStringLiteral("ddddd")));
	}
	auto versionList = mapVersionFields[QStringLiteral("文件版本")]->text().trimmed().split(".");
	updater.VersionInfo->FixedFileInfo.dwFileVersionMS = MAKELONG(versionList[1].toInt(), versionList[0].toInt());
	updater.VersionInfo->FixedFileInfo.dwFileVersionLS = MAKELONG(versionList[3].toInt(), versionList[2].toInt());
	if (updater.Update()) 
	{
		QMessageBox::information(this, QStringLiteral("修改版本信息"), QStringLiteral("修改成功"), QDialogButtonBox::Yes);
	}
	else 
	{
		QMessageBox::warning(this, QStringLiteral("修改版本信息"), QStringLiteral("修改失败"));
	}
}
