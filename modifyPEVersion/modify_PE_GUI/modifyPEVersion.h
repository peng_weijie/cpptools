#pragma once

#include <QtWidgets/QMainWindow>
#include "QMap"
#include "CVersionUpdater.h"
class QLabel;
class QLineEdit;
class QPushButton;
class QString;
class QTabWidget;
class QVBoxLayout;
class modifyPEVersion : public QMainWindow
{
    Q_OBJECT

public:
    modifyPEVersion(QWidget *parent = Q_NULLPTR);
private:
	void initVersionUI();
	void initVersionMap();
	void initEachLineUI(QVBoxLayout *pMainLayout, QString lableName);
	void init1stLineUI(QVBoxLayout *pMainLayout);
	void initLastLineUI(QVBoxLayout *pMainLayout);
public slots:
	void onClickedOpenFile();
	void onModifyVersionClicked();
private:
	//business members
	QString m_strFileDir;
	CVersionUpdater updater;
	//UI members
	QTabWidget *m_pTabWidget;
	QLineEdit *m_pLinePath;
	
	QMap<QString, QLineEdit*> mapVersionFields;
};
