#include<iostream>
#include "CVersionUpdater.h"
#include "cmdline.h"
using namespace std;

void CharToTchar(const char * _char, TCHAR * tchar)
{
	int iLength;

	iLength = MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, NULL, 0);
	MultiByteToWideChar(CP_ACP, 0, _char, strlen(_char) + 1, tchar, iLength);
}

void parserParaments(cmdline::parser& parser, int argc, char **argv)
{
	parser.add<std::string>("filePath", '\0', "二进制文件路径", true, "");
	parser.add<std::string>("productName", '\0', "产品名称", false, "");
	parser.add<std::string>("productVersion", '\0', "产品版本，支持字符串类型", false, "");
	parser.add<std::string>("company", '\0', "公司名称", false, "");
	parser.add<std::string>("inner", '\0', "软件内部名称", false, "");
	parser.add<std::string>("origin", '\0', "软件原始名称", false, "");
	parser.add<std::string>("trademark", '\0', "合法商标", false, "");
	parser.add<std::string>("comment", '\0', "备注", false, "");
	parser.add<std::string>("description", '\0', "文件描述", false, "");
	parser.add<std::string>("version", '\0', "文件版本，只支持4位数字（111.2222222.3333333.44444）", false, "");
	parser.add<std::string>("copyright", '\0', "版权", false, "");
	if (!parser.parse(argc, argv))
	{
		std::cerr << parser.usage();
		exit(-1);
	}
}

vector<std::string> split(const std::string& str, const std::string& delim) {
	vector<std::string> res;
	if ("" == str) return res;
	//先将要切割的字符串从string类型转换为char*类型
	char * strs = new char[str.length() + 1]; //不要忘了
	strcpy(strs, str.c_str());

	char * d = new char[delim.length() + 1];
	strcpy(d, delim.c_str());

	char *p = strtok(strs, d);
	while (p) {
		std::string s = p; //分割得到的字符串转换为string类型
		res.push_back(s); //存入结果数组
		p = strtok(NULL, d);
	}

	return res;
}


int main(int argc, char **argv)
{
	cmdline::parser parser;
	parserParaments(parser, argc, argv);

	CVersionUpdater updater;
	if (updater.Open(parser.get<std::string>("filePath").c_str()))
	{
		if (updater.VersionInfo == nullptr)
		{
			updater.VersionInfo = new CVersionInfo;
		}
		if (updater.VersionInfo->StringFileInfo == NULL)
			updater.VersionInfo->StringFileInfo = new CStringFileInfo;
		if (updater.VersionInfo->StringFileInfo->Children.size() < 0)
			updater.VersionInfo->StringFileInfo->Children.push_back(new CStringTable);
		TCHAR tchar[256];
		for (CStringTable* st : updater.VersionInfo->StringFileInfo->Children)
		{
			if (parser.exist("productName"))
			{
				CharToTchar(parser.get<std::string>("productName").c_str(), tchar);
				st->SetProductName(tchar);
			}
			if (parser.exist("productVersion"))
			{
				CharToTchar(parser.get<std::string>("productVersion").c_str(), tchar);
				st->SetProductVersion(tchar);
			}
			if (parser.exist("company"))
			{
				CharToTchar(parser.get<std::string>("company").c_str(), tchar);
				st->SetCompanyName(tchar);
			}
			if (parser.exist("inner"))
			{
				CharToTchar(parser.get<std::string>("inner").c_str(), tchar);
				st->SetInternalName(tchar);
			}
			if (parser.exist("origin"))
			{
				CharToTchar(parser.get<std::string>("origin").c_str(), tchar);
				st->SetOriginalFilename(tchar);
			}
			if (parser.exist("trademark"))
			{
				CharToTchar(parser.get<std::string>("trademark").c_str(), tchar);
				st->SetLegalTrademarks(tchar);
			}
			if (parser.exist("comment"))
			{
				CharToTchar(parser.get<std::string>("comment").c_str(), tchar);
				st->SetComments(tchar);
			}
			if (parser.exist("description"))
			{
				CharToTchar(parser.get<std::string>("description").c_str(), tchar);
				st->SetFileDescription(tchar);
			}
			if (parser.exist("version"))
			{
				CharToTchar(parser.get<std::string>("version").c_str(), tchar);
				st->SetFileVersion(tchar);
			}
			if (parser.exist("copyright"))
			{
				CharToTchar(parser.get<std::string>("copyright").c_str(), tchar);
				st->SetLegalCopyright(tchar);
			}

			// 			st->SetPrivateBuild(_T(""));
			// 			st->SetSpecialBuild(_T(""));
		}
		if (parser.exist("version"))
		{
			auto fileVersion = parser.get<std::string>("version");
			int count = 0;
			auto versions = split(fileVersion, ".");
			std::string::size_type sz;
			int v0 = std::stoi(versions[0], &sz);
			int v1 = std::stoi(versions[1], &sz);
			int v2 = std::stoi(versions[2], &sz);
			int v3 = std::stoi(versions[3], &sz);
			updater.VersionInfo->FixedFileInfo.dwFileVersionMS = MAKELONG(v1, v0);
			updater.VersionInfo->FixedFileInfo.dwFileVersionLS = MAKELONG(v3, v2);
			updater.VersionInfo->FixedFileInfo.dwProductVersionMS = MAKELONG(v1, v0);
			updater.VersionInfo->FixedFileInfo.dwProductVersionLS = MAKELONG(v3, v2);
		}
		if (updater.Update())
		{
			cout << "update " << parser.get<std::string>("filePath") <<" success" << endl;
		}
		else
		{
			cout << "update " << parser.get<std::string>("filePath") << " fail" << endl;
		}
	}
	else
	{
		cout << "could not open " << parser.get<std::string>("filePath") << endl;
	}
}