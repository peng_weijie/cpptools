#pragma once
#include "CVersionInfo.h"
#include <atlstr.h>
class CVersionUpdater
{
public:
	CVersionUpdater();
	~CVersionUpdater();

public:
	bool Open(const ATL::CString& exeOrDll);
	bool Update(WORD language_ = MAKELANGID(LANG_NEUTRAL, SUBLANG_NEUTRAL)) const;

public:
	CVersionInfo* VersionInfo;

private:
	ATL::CString ExeOrDll;
};

