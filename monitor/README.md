# Description:
A simple cpp tools to monitor the system and processes cpu and memory usage.


note: This tools can only be used in Linux.

# Dependencies
## libraries
- tcmalloc
- curses
- c++17 or later
## tools or commands
- cmake(3.16 or later)
- docker
- ps
- grep
- pushd & popd
- gdb(optional)

# Build
## build the release version
```shell
bash build.sh release
```
## build the debug version
```shell
bash build.sh debug
```

# Usage:
```shell
bash build.sh run
```