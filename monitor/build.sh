#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

function make_build() {
    pushd "$DIR/build" >/dev/null 2>&1 || exit
    make monitor
    popd >/dev/null 2>&1 || exit

}

function release() {
    pushd "$DIR" >/dev/null 2>&1 || exit
    cmake -S . -B build
    make_build
    popd >/dev/null 2>&1 || exit
}

function debug() {
    pushd "$DIR" >/dev/null 2>&1 || exit
    cmake -DCMAKE_BUILD_TYPE=Debug -S . -B build
    make_build
    popd >/dev/null 2>&1 || exit
}

function run() {
    "$DIR/build/bin/monitor"
}

function gdb() {
    $(which gdb) "$DIR/build/bin/monitor"
}

"$@"