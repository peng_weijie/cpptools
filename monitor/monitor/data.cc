#include "data.h"
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <filesystem>
using namespace std;

static const char *const RED = "\033[31m";
static const char *const GREEN = "\033[32m";
static const char *const NO_COLOR = "\033[0m";
static const char *const BOLD = "\033[1m";
static const char *const SaveToFileName = "save.txt";

void DataInfoMgr::SaveToFile() {
    auto path = filesystem::current_path().assign(SaveToFileName);
  ofstream ofs(path.c_str(), std::ios::app);
  if (!ofs.is_open()) {
    cerr << "Failed to open file: " << filesystem::current_path() << SaveToFileName;
    return;
  }
  for (auto &strs : this->AsString()) {
    for (auto &str : strs) {
      ofs << str << "\t";
    }
    ofs << endl;
  }
  ofs.close();
}

template <typename T>
string NumberToString(const T &number, int precision = 3) {
  stringstream ss;
  ss << setprecision(precision) << number;
  return ss.str();
}

vector<vector<string>> SystemInfoMgr::AsString() {
  std::shared_lock<std::shared_mutex> lck(mutex_);
  vector<vector<string>> res(2);
  res[0].push_back("total");
  res[0].push_back(NumberToString(system_info_.total / 1024.0 / 1024, 4) +
                   " GB");
  res[1].push_back("available");
  res[1].push_back(NumberToString(system_info_.available / 1024.0 / 1024, 4) +
                   " GB");
  return res;
}

void ProcessInfoMgr::UpdateProcessInfo(int64_t pid, const string &ns,
                                       const string &process_name,
                                       int64_t current, float pcpu) {
  std::unique_lock<std::shared_mutex> lck(mutex_);

  process_memory_map_[pid].current = current;
  process_memory_map_[pid].pid = pid;
  process_memory_map_[pid].pcpu = pcpu;
  process_memory_map_[pid].process_name = process_name;
  process_memory_map_[pid].ns = ns;
  if (process_memory_map_[pid].max < current) {
    process_memory_map_[pid].max = current;
  }
}

vector<vector<string>> ProcessInfoMgr::AsString() {

  std::shared_lock<std::shared_mutex> lck(mutex_);
  vector<ProcessInfo> values;
  for (auto &element : process_memory_map_) {
    values.push_back(element.second);
  }
  sort(values.begin(), values.end(),
       [](const ProcessInfo &a, const ProcessInfo &b) {
         return a.current > b.current || a.max > b.max;
       });
  int size = min(static_cast<size_t>(max_display_process_), values.size());
  vector<vector<string>> res(size + 1);
  res[0].push_back("namespace");
  res[0].push_back("pid");
  res[0].push_back("name");
  res[0].push_back("current");
  res[0].push_back("max");
  res[0].push_back("cpu");
  for (int i = 0; i < size; ++i) {
    auto &process_info = values[i];
    res[i + 1].push_back(process_info.ns);
    res[i + 1].push_back(NumberToString(process_info.pid));
    res[i + 1].push_back(process_info.process_name);
    res[i + 1].push_back(NumberToString(process_info.current / 1024.0 / 1024));
    res[i + 1].push_back(NumberToString(process_info.max / 1024.0 / 1024));
    res[i + 1].push_back(NumberToString(process_info.pcpu));
  }
  return res;
}

void ProcessInfoMgr::CleanDeprecated(const vector<int64_t> &live) {
  std::unique_lock<std::shared_mutex> lck(mutex_);
  for (auto it = process_memory_map_.begin();
       it != process_memory_map_.end();) {
    if (std::find(live.begin(), live.end(), it->first) == live.end()) {
      it = process_memory_map_.erase(it);
    } else {
      ++it;
    }
  }
}