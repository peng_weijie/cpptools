#pragma once
#include <atomic>
#include <iostream>
#include <mutex>
#include <shared_mutex>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;
struct ProcessInfo {
  int64_t pid = -1;
  string ns;
  string process_name;
  // 最大内存
  int64_t max = -1;
  // 最近一次内存值
  int64_t current = 0;
  float pcpu = 0.0;
};

struct SystemInfo {
  int64_t total = 0;
  // 最近一次内存值
  int64_t available = 0;
};

class DataInfoMgr {
public:
  virtual vector<vector<string>> AsString() = 0;
  void SaveToFile();
};

// 单例
class SystemInfoMgr : public DataInfoMgr {
public:
  static SystemInfoMgr &GetInstance() {
    static SystemInfoMgr instance;
    return instance;
  }
  vector<vector<string>> AsString() override;

  inline void UpdateSystemInfo(int64_t total, int64_t available) {
    std::unique_lock<std::shared_mutex> lck(mutex_);
    system_info_.available = available;
    system_info_.total = total;
  }

private:
  SystemInfoMgr() = default;
  ~SystemInfoMgr() = default;
  SystemInfoMgr(const SystemInfoMgr &) = delete;
  SystemInfoMgr &operator=(const SystemInfoMgr &) = delete;

private:
  SystemInfo system_info_;
  mutable std::shared_mutex mutex_;
};

// 单例
class ProcessInfoMgr : public DataInfoMgr {
public:
  static ProcessInfoMgr &GetInstance() {
    static ProcessInfoMgr instance;
    return instance;
  }
  vector<vector<string>> AsString() override;

  void UpdateProcessInfo(int64_t pid, const string &ns,
                         const string &process_name, int64_t current,
                         float pcpu);
  void CleanDeprecated(const vector<int64_t> &live);
  inline void SetMaxDisplayProcess(int64_t max_display_process) {
    std::unique_lock<std::shared_mutex> lck(mutex_);
    max_display_process_ = max_display_process;
  }

private:
  ProcessInfoMgr() = default;
  ~ProcessInfoMgr() = default;
  ProcessInfoMgr(const ProcessInfoMgr &) = delete;
  ProcessInfoMgr &operator=(const ProcessInfoMgr &) = delete;

private:
  int64_t max_display_process_ = 10;
  unordered_map<int64_t, ProcessInfo> process_memory_map_;
  mutable std::shared_mutex mutex_;
};

class GlobalThreadMgr {
public:
  static GlobalThreadMgr &GetInstance() {
    static GlobalThreadMgr instance;
    return instance;
  }
  inline bool IsRunning() const { return !stop_; }
  inline void SetIsRunning(bool is_running) { stop_ = !is_running; }

private:
  GlobalThreadMgr() = default;
  ~GlobalThreadMgr() = default;
  GlobalThreadMgr(const GlobalThreadMgr &) = delete;
  GlobalThreadMgr &operator=(const GlobalThreadMgr &) = delete;

  atomic_bool stop_{false};
};
