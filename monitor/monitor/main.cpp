#include "data.h"
#include "monitor.h"
#include "terminal.h"
#include <memory>
using namespace std;

int main() {
  unique_ptr<Terminal> terminal = make_unique<Terminal>();
  terminal->SetPeriod(500);
  terminal->Work();

  unique_ptr<Monitor> monitor = make_unique<Monitor>();
  monitor->SetPeriod(500);
  monitor->Work();
  return 0;
}