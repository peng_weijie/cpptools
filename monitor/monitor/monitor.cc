#include "monitor.h"
#include "data.h"
#include <cctype>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>
using namespace std;

Monitor::Monitor() : Worker() {}

Monitor::~Monitor() {}

static const char *const MEM_INFO_FILE = "/proc/meminfo";

static inline void ltrim(std::string &s) {
  s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
            return !std::isspace(ch);
          }));
}

static inline void rtrim(std::string &s) {
  s.erase(std::find_if(s.rbegin(), s.rend(),
                       [](unsigned char ch) { return !std::isspace(ch); })
              .base(),
          s.end());
}

static inline string trim(std::string &s) {
  rtrim(s);
  ltrim(s);
  return s;
}

void Monitor::MonitorSystemInfo() {
  std::ifstream meminfo(MEM_INFO_FILE);
  if (!meminfo) {
    cerr << "Open " << MEM_INFO_FILE << " failed!";
    exit(1);
  }
  std::string line;
  int set_value_cnt = 0;
  int64_t total = 0, available = 0;
  while (std::getline(meminfo, line)) {
    std::istringstream iss(line);
    std::string name;
    int64_t value;
    iss >> name >> value;
    if (name == "MemTotal:") {
      total = value;
      set_value_cnt++;
    } else if (name == "MemAvailable:") {
      available = value;
      set_value_cnt++;
    }
    if (set_value_cnt == 2) {
      break;
    }
  }
  SystemInfoMgr::GetInstance().UpdateSystemInfo(total, available);
}
void Monitor::MonitorProcessesInfo() {
  string cmd = "ps -eo pid,comm,cgroup,rsz,pcpu --sort -rsz | grep -v grep | "
               "grep -wv RSZ";
  auto top_info = RunCommand(cmd);
  istringstream iss(*top_info);
  string s;
  vector<int64_t> lived_processes;
  while (getline(iss, s, '\n')) {
    stringstream ss(s);
    int64_t pid;
    string comm;
    string cgroup;
    int64_t rsz;
    float pcpu;
    lived_processes.push_back(pid);
    ss >> pid >> comm >> cgroup >> rsz >> pcpu;
    ProcessInfoMgr::GetInstance().UpdateProcessInfo(pid, ParseNamespace(cgroup),
                                                    trim(comm), rsz, pcpu);
  }
  ProcessInfoMgr::GetInstance().CleanDeprecated(lived_processes);
}

optional<string> Monitor::RunCommand(const string &command) {
  constexpr auto deleter = [](FILE *f) {
    if (f)
      pclose(f);
  };
  std::unique_ptr<FILE, decltype(deleter)> pipe(popen(command.c_str(), "r"),
                                                deleter);
  optional<string> ret = std::nullopt;
  if (!pipe) {
    return ret;
  }
  char buffer[128] = {};
  while (!feof(pipe.get())) {
    if (fgets(buffer, 128, pipe.get()) != NULL) {
      if (ret) {
        ret->append(buffer);
      } else {
        ret.emplace(buffer);
      }
    }
  }
  return ret;
}

string Monitor::ParseNamespace(const std::string &cgroup) {
  int idx = cgroup.find("/user.slice", 0);
  if (idx >= 0) {
    return "user";
  }
  idx = cgroup.find("/system.slice", 0);
  if (idx >= 0) {
    return "system";
  }
  string docker_group = "/docker/";
  idx = cgroup.find(docker_group, 0);
  if (idx >= 0) {
    string docker_id = cgroup.substr(idx + docker_group.length());
    auto inspect_ret =
        RunCommand("docker inspect " + docker_id + " --format '{{.Name}}'");
    if (inspect_ret.has_value()) {
      return (*inspect_ret).substr(1);
    }
  }
  return "UNKNOWN";
}

void Monitor::Work() {
  while (GlobalThreadMgr::GetInstance().IsRunning()) {
    MonitorSystemInfo();
    MonitorProcessesInfo();
    if (period_ > 0) {
      std::this_thread::sleep_for(std::chrono::milliseconds(period_));
    } else {
      std::this_thread::yield();
    }
  }
}
