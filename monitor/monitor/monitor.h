#pragma once
#include "memory"
#include "terminal.h"
#include "worker.h"
#include <optional>
#include <string>
#include <unordered_map>
using namespace std;
class Monitor final : public Worker {
public:
  Monitor();
  ~Monitor();
  virtual void Work() override;

private:
  void DoMonitor();
  void MonitorSystemInfo();
  void MonitorProcessesInfo();
  optional<string> RunCommand(const string &command);
  string ParseNamespace(const std::string &cgroup);
};