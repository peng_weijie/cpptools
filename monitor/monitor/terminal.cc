#include "data.h"
#include "ncurses.h"
#include <cctype>
#include <iostream>
#include <terminal.h>

using namespace std;

void Terminal::onQuit() { GlobalThreadMgr::GetInstance().SetIsRunning(false); }

void Terminal::onCapture() {
  SystemInfoMgr::GetInstance().SaveToFile();
  ProcessInfoMgr::GetInstance().SaveToFile();
}

void Terminal::onSort() {}
void Terminal::onFilter() {}

#define MAKE_STD_FUNTION(name)                                                 \
  static_cast<function<void()>>(bind(&Terminal::name, this))

bool Terminal::IsInited() { return stdscr != nullptr; }

Terminal::Terminal() { Init(); }

Terminal::~Terminal() {
  echo();
  endwin();
}

void Terminal::Init() {
  initscr();
  if (stdscr == nullptr) {
    cerr << "Failed to initialize ncurses" << endl;
    return;
  }
  nodelay(stdscr, true);
  keypad(stdscr, true);
  meta(stdscr, true);
  curs_set(0);
  noecho();
  refresh();
  clear();
  RegisterKeyEvent('c', MAKE_STD_FUNTION(onCapture));
  RegisterKeyEvent('s', MAKE_STD_FUNTION(onSort));
  RegisterKeyEvent('f', MAKE_STD_FUNTION(onFilter));
  RegisterKeyEvent('q', MAKE_STD_FUNTION(onQuit));
}

void print(const vector<vector<string>> &data) {
  vector<size_t> width(200, 0);
  for (int i = 0; i < data.size(); i++) {
    for (int j = 0; j < min(data[i].size(), size_t(20)) - 1; j++) {
      width[j + 1] = max(width[j + 1], data[i][j].size() + width[j] + 5);
    }
  }
  clear();
  for (int i = 0; i < data.size(); i++) {
    for (int j = 0; j < data[i].size(); j++) {
      mvaddstr(i, width[j], data[i][j].c_str());
    }
  }
}

void Terminal::Print() {
  if (!IsInited()) {
    cerr << "Terminal is not initialized" << endl;
    return;
  }

  while (GlobalThreadMgr::GetInstance().IsRunning()) {
    auto system_info = SystemInfoMgr::GetInstance().AsString();
    auto process_info = ProcessInfoMgr::GetInstance().AsString();
    vector<vector<string>> print_info;
    print_info.insert(print_info.end(), system_info.begin(), system_info.end());
    print_info.emplace_back(vector<string>(1, ""));
    print_info.insert(print_info.end(), process_info.begin(),
                      process_info.end());
    print(print_info);
    if (period_ > 0) {
      std::this_thread::sleep_for(std::chrono::milliseconds(period_));
    } else {
      std::this_thread::yield();
    }
  }
}

void Terminal::Work() {
  interact_thread_ = thread(&Terminal::HandleKeyboardEvent, this);
  interact_thread_.detach();
  output_thread_ = thread(&Terminal::Print, this);
  output_thread_.detach();
}

void Terminal::RegisterKeyEvent(char key, std::function<void()> callback) {
  if (isalpha(key)) {
    func_map_[toupper(key)] = callback;
    func_map_[tolower(key)] = callback;
  } else {
    func_map_[key] = callback;
  }
}

void Terminal::HandleKeyboardEvent() {
  while (GlobalThreadMgr::GetInstance().IsRunning()) {
    int key = getch();
    if (func_map_.count(key) > 0) {
      func_map_[key]();
    }
    std::this_thread::yield();
  }
}