#pragma once
#include <functional>
#include <thread>
#include <unordered_map>
#include <worker.h>
class Terminal final : public Worker {
public:
  Terminal();
  virtual ~Terminal();
  virtual void Work() override;
  void RegisterKeyEvent(char key, std::function<void()> callback);
  void Print();
private:
  void Init();
  bool IsInited();
  void HandleKeyboardEvent();

private:
  void onQuit();
  void onCapture();
  void onSort();
  void onFilter();

private:
  unordered_map<char, std::function<void()>> func_map_;
  thread output_thread_;
  thread interact_thread_;
};