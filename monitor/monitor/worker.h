#pragma once
#include <cstdint>
using namespace std;

class Worker {
public:
  Worker() = default;
  virtual ~Worker() {}
  inline void SetPeriod(int64_t period) { period_ = period; }
  virtual void Work() = 0;

protected:
  int64_t period_ = -1;
};